/*
    Lista apenas o nome e sprites dos pokemons
*/

const data = require('../data')

module.exports = function(req, res){

    var result = [];

    data.forEach(poke => {
        result.push({
            name:poke.name,
            sprites: poke.sprites
        })
    });


    res.json(result)
}